DESIGNATIONS:----------------------
framework: Akshay + Sartaj

umleditor: Edgar + Stephen

HTML Table:------------------------
Layer 1: Nodes
Layer 2: Connectors




REPORT FORMAT-----------------------

MAKE THESE IN REPORT FOLDER:
filename: [lastname][firstname INTIAL]-week[Week#].txt

Week x
======
[Date] [time1]-[time2]: [What you worked on]
[Date] [time3]-[time4]: [What you worked on]
...
Comments: [comments]
Total hours this week: [hoursWorked]
Total hours on project:[totalHoursWorked]


PROJECT INFO:-----------------------
TEAM NAME:		Victor
DIAGRAM TYPE:	Object
HARD OBJ/PROTO:	Hard Objects
SVG/CANVAS:		Canvas
HTML TABLES:	Yes
Section:		6, T/Th 3:00pm - 4:15pm

Team:			Stephen Liu
				Sartaj Singh
				Akshay Suda
				Edgar Velasquez

GUIDELINES:-------------------------
Project teams will generally be composed of four students.

I form the teams. You can tell me in Homework 8 who your preferred team members are. Add a file project.txt to your submission with their names (first and last).

All members of a team must attend the same section.

The general assignment for all teams is to produce a graph editor framework in JavaScript and demonstrate the framework concept by producing a simple editor (circle nodes, diamond nodes, a couple of line styles) and a UML editor (class, object, or sequence diagrams).

It is expected that half of the team work on the framework (including the simple editor instance) and the other half on the UML editor.

To the degree that it is practical, use as much code as you can from the classic Violet UML editor (just unzip the Violet JAR file). This is particularly important for the sequence diagram—there is a fair amount of intricate logic that would be difficult to rediscover in a short time span.

The basic functionality should be like in the classic Java Violet—buttons for nodes and edges, dragging of nodes and connecting edges with the mouse, some kind of property editor dialog.

Each team will receive specific instructions about implementation strategies it must use.

As a part of the project, each team is expected to reflect on the use of OO design and patterns in their project.

Each team maintains a private Git repo, adding me and the grader as collaborators. This is different from your personal repo.

Put all documentation in the Github/BitBucket Wiki. If it's not in the Wiki, I won't look at it.

Each week (or more often), append to a list of tasks in the Wiki. Each task has a single owner. Mark tasks as completed/abandoned when appropriate. Don't delete completed/abandoned tasks.

Each team member makes a Git commit at least 5 times per week.

Each team is prepared to give a brief progress report in class each Monday or Tuesday, with every member present.

Each team member submits an individual report each Sunday night in the project Wiki.

All project code is always on Git.

It must build on my machine (and on yours).

Be prepared to give a presentation with your architectural choices, UML design, and design patterns usage, at the end of the semester.


