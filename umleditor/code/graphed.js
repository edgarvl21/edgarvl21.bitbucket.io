'use strict'

function drawGrabber(x, y) {
  const size = 5;
  const canvas = document.getElementById('graphpanel')
  const ctx = canvas.getContext('2d')
  ctx.beginPath()
  ctx.fillStyle = 'black'
  ctx.fillRect(x-size/2, y-size/2, size,size)
}

function createNode(x, y, size, color, elementID, name, attributes) {
  return {
      setElementID: (newElementID) => {
          elementID = newElementID
      },
      setText: (newName, newAttributes) => {
          name = newName
          attributes = newAttributes
      },
      getBounds: () => {
          return {
              x: x,
              y: y,
              width: size,
              height: size
          }
      },
      contains: p => {
          return (x + size / 2 - p.x) ** 2 + (y + size / 2 - p.y) ** 2 <= size ** 2 / 4
      },
      translate: (dx, dy) => {
          x += dx
          y += dy
      },
      draw: () => {
          var body = document.getElementById(elementID)
          var table = document.createElement('table')
          table.style.position = 'absolute'
          table.style.backgroundColor = color
          table.style.left = x
          table.style.top = y
          table.width = size

          var tableBody = document.createElement('tbody')
          table.appendChild(tableBody)

          var tr = document.createElement('tr')
          var th = document.createElement('th')
          if (name === undefined){
              th.innerText = 'Object Name'
          } else {
              th.innerText = name
          }
          tr.appendChild(th)
          tableBody.appendChild(tr)

          var tr2 = document.createElement('tr')
          var th2 = document.createElement('th')
          if (attributes === undefined){
              th2.innerText = 'Attributes'
          } else {
              th2.innerText = attributes
          }
          tr2.appendChild(th2)
          tableBody.appendChild(tr2)

          body.appendChild(table)
      }
  }
}
function createCircleNode (x, y, size, color) {
  return {
    getBounds: () => {
      return {
        x: x,
        y: y,
        width: size,
        height: size
      }
    },
    contains: p => {
      return (x + size / 2 - p.x) ** 2 + (y + size / 2 - p.y) ** 2 <= size ** 2 / 4
    },
    
    translate: (dx, dy) => {
      if(y+dy>=50){
      x += dx
      y += dy
      }
    },
    getColor : () =>{
      return color;
    },
    // draw: () => {
    //   const canvas = document.getElementById('graphpanel')
    //   const ctx = canvas.getContext('2d')
    //   ctx.beginPath()
    //   ctx.arc(x, y, size/2, 0, 360, false)
    //   ctx.fillStyle = color
    //   ctx.fill()
    // }
    draw(){
      const container = document.getElementById('nodeContainer')
      const table = document.createElement('table')
      const tr = document.createElement('tr')
      const td = document.createElement('td')
      
      table.appendChild(tr)
      tr.appendChild(td)
      
      table.style.position = 'absolute'
      table.style.left = x + 'px'
      table.style.top = y + 'px'
      table.style.width = size + 'px'
      table.style.height = size + 'px'
      table.style.background = color
      container.appendChild(table)


    },
    getType : () =>{
      return 'circle'
    }
  }
}



function createToolBar(){
  let nodes = [];
  return{
    draw : () =>{
    const canvas = document.getElementById('graphpanel')
    const ctx = canvas.getContext('2d')
    ctx.beginPath()
    ctx.strokeRect(0, 0, 400, 50)
      for (const n of nodes) {
	  if (n.getType() === 'circle') {
	      n.draw()
	  }
	  else if (n.getType() === 'edge') {
	      n.drawTool()
	  }
      }
    },
    add: (n) =>{
      nodes.push(n)
    },
    findNode: (p) => {
	for (const n of nodes) {
	    if (n.contains(p)) return n
	}
	return undefined
    },
  }
}

class Graph {
  constructor() {
    this.nodes = []
    this.edges = []
  }
  add(n) {
    this.nodes.push(n)
  }
  findNode(p) {
    for (let i = this.nodes.length - 1; i >= 0; i--) {
      const n = this.nodes[i]
      if (n.contains(p)) return n
    }
    return undefined
  }
  draw() {
    for (const n of this.nodes) {
      n.draw()
    }
    for (const e of this.edges) {
      e.draw()
    }
  }

  connect(e, p1, p2) {
    const n1 = this.findNode(p1)
    const n2 = this.findNode(p2)
    if (n1 !== undefined && n2 !== undefined) {
      e.connect(n1, n2)
      this.edges.push(e)
      return true
    }
    return false
  }
}
function center(rect) {
  return { x: rect.x + rect.width / 2, y: rect.y + rect.height / 2 }
}

function createLineEdge() {
  let start = undefined
  let end = undefined
  return {
    connect: (s, e) => {
      start = s
      end = e
    },
    draw: () => {
      const canvas = document.getElementById('graphpanel')
      const ctx = canvas.getContext('2d')
      ctx.beginPath()
      // Just pick the center of the bounds for now
      const p = center(start.getBounds())
      // Not the "connection points" that graphed2 uses
      const q = center(end.getBounds()) 
      ctx.moveTo(p.x, p.y)
      ctx.lineTo(q.x, q.y)
      ctx.stroke()
    },
    getType: () =>{
      return 'edge'
    }
    ,
    drawTool: () => {
	const canvas = document.getElementById('graphpanel')
	const ctx = canvas.getContext('2d')
        ctx.beginPath()
        ctx.moveTo(150, 35)
	ctx.lineTo(120, 15)
	ctx.closePath();
	ctx.stroke();

    },
      contains: (p) => {
	  
      }
  }
}
function createRectangleNode (x, y, width1,height1, color,words) {
  let array =[];
  return {
    getBounds: () => {
      return {
        x: x+50,
        y: y,
        width: width1,
        height: height1
        
      }
    },
    contains: p => {
      return (x + width1 / 2 - p.x) ** 2 + (y + width1 / 2 - p.y) ** 2 <= width1 ** 2 / 4
    },
    translate: (dx, dy) => {
      x += dx
      y += dy
    },
    
    addText: (newText) => {
      array.push(newText)

    },
    draw(){
      const container = document.getElementById('nodeContainer')
      const table = document.createElement('table')
      var tblBody = document.createElement("tbody");

     // const tr = document.createElement('tr')
      //const td = document.createElement('td')
      //array.push(words)
      for (let i = 0;i<array.length ; i++) {
        var tr = document.createElement("tr");
        var td = document.createElement("td");
        const tempText = document.createTextNode(array[i])
       // td.appendChild(tempText)
       td.appendChild(tempText);
      tr.appendChild(td);
      table.appendChild(tr)
      }
      


     // td.id='rectangle1'
     // table.appendChild(tr)
     // tr.appendChild(td)
      table.style.position = 'absolute'
      table.style.left = x + 'px'
      table.style.top = y + 'px'
      table.style.width = width1 + 'px'
      table.style.height = height1 + 'px'
      table.style.background = color
      container.appendChild(table)

      var n=words.length
      //FontRenderContext context = new FontRenderContext() {};
      //Rectangle2D bounds = font.getStringBounds(text, context);
      
      // bounds.getWidth() and bounds.getHeight() give the width and height
      // of the string in the given font
      
      //g2.setFont(font);
      //g2.drawString(string, (int) x, (int) (y - bounds.getY()));
   
      // draws the string with upper left corner (x, y)
    },
    getType : () =>{
      return 'parent'
    }
  }
}

function createTextNode (x, y, width1,height1, color) {
  return {
    getBounds: () => {
      return {
        x: x,
        y: y,
        width: width1,
        height: height1
      }
    },
    contains: p => {
      return (x + width1 / 2 - p.x) ** 2 + (y + width1 / 2 - p.y) ** 2 <= width1 ** 2 / 4
    },
    translate: (dx, dy) => {
      x += dx
      y += dy
    },
    draw: () => {
		

	const canvas = document.getElementById("graphpanel");
  const ctx = canvas.getContext("2d");
  ctx.font = "20px Arial";
  ctx.fillStyle = color;
  ctx.fillText("Name = value", 100, 100); 
  
},
getType : () =>{
  return 'child'
}
  }
}
document.addEventListener('DOMContentLoaded', function () {
  const graph = new Graph()
  
  const rect1=createRectangleNode (10, 20, 30,20, 'grey',"txt")
  const rect2=createRectangleNode (10, 20, 30,20, 'red',"txt")
  const n3 = createNode(100, 100, 100, 'ligthgrey', 'nodeContainer')

  const e = createLineEdge()
  
  graph.add(rect1)
  graph.add(rect2)
  //graph.add(n3)

  //graph.connect(e, { x: 20, y: 20 }, { x: 40, y: 40 })
rect2.addText("New")
rect2.addText("yei")
rect2.addText("The longest path")
const text1=createTextNode (10, 10, 10,10, 'black')
  graph.draw()
 // rect2.addText("New")
  //ADDING TOOLS
  const blueCircle = createCircleNode(20, 12, 30, 'blue')
  const goldenCircle = createCircleNode(70, 12, 30, 'goldenrod')
  const edgeTool = createLineEdge()
 
  const tools = createToolBar()
  tools.add(blueCircle)
  tools.add(goldenCircle)
  tools.add(edgeTool)

  //const canvas = document.getElementById('rectangle1')
  
  graph.draw()
  tools.draw()
  //const e = createLineEdge()
  //graph.connect(e, { x: 60, y: 60 }, { x: 80, y: 80 })
  
  const panel = document.getElementById('graphpanel')
  let selected = undefined
  let dragStartPoint = undefined
  let dragStartBounds = undefined

  function repaint() {
    const container = document.getElementById('nodeContainer')
    const canvas = document.getElementById('graphpanel')
    const ctx = canvas.getContext('2d')
    ctx.clearRect(0, 0, canvas.width, canvas.height)
    container.innerHTML = ''
    graph.draw()
    tools.draw()
    if (selected !== undefined) {
      const bounds = selected.getBounds()
      drawGrabber(bounds.x, bounds.y)
      drawGrabber(bounds.x + bounds.width, bounds.y)
      drawGrabber(bounds.x, bounds.y + bounds.height)      
      drawGrabber(bounds.x + bounds.width, bounds.y + bounds.height)
    }
      
  }
  
  function mouseLocation(event) {
    var rect = panel.getBoundingClientRect();
    return {
      x: event.clientX - rect.left,
      y: event.clientY - rect.top,
    }
  }
  
  panel.addEventListener('mousedown', event => {
    let mousePoint = mouseLocation(event)
    selected = graph.findNode(mousePoint)
    if (selected !== undefined) {
      dragStartPoint = mousePoint
      dragStartBounds = selected.getBounds()
    }
    else {
	selected = tools.findNode(mousePoint)
	if (selected !== undefined) {
            if ('circle' === selected.getType()) { // if the tools.findNode function returns a node in the toolbar
	        //selected = undefined // uncomment this to get rid of grabbers when toolbar node is clicked on
	        //if(selected.getColo)
		const newNode = createCircleNode(200, 200, 20, selected.getColor())   // creates a new node, this is just placeholder
		graph.add(newNode)                                       // ideally in the future we can figure out the kind of node then create it
		repaint()
	    }
	}
  /*if ('edge' === selected.getType()){
    const e = createLineEdge()
    graph.connect(e, { x: 60, y: 60 }, { x: 80, y: 80 })
  }*/
    }
    repaint()
  })

  panel.addEventListener('mousemove', event => {
    if (dragStartPoint === undefined) return
    let mousePoint = mouseLocation(event)
    if (selected !== undefined) {
      const bounds = selected.getBounds();
      
      selected.translate(
        dragStartBounds.x - bounds.x 
          + mousePoint.x - dragStartPoint.x,
        dragStartBounds.y - bounds.y 
          + mousePoint.y - dragStartPoint.y);
      repaint()
    }
  })
  
  panel.addEventListener('mouseup', event => {
    dragStartPoint = undefined
    dragStartBounds = undefined
  })
})
