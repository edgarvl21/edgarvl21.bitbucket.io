'use strict'

function drawGrabber (x, y) {
  const size = 5
  const canvas = document.getElementById('graphpanel')
  const ctx = canvas.getContext('2d')
  ctx.beginPath()
  ctx.fillStyle = 'black'
  ctx.fillRect(x - size / 2, y - size / 2, size, size)
}
function createCircleNode (x, y, size, color) {
  return {
    getBounds: () => {
      return {
        x: x,
        y: y,
        width: size,
        height: size
      }
    },
    contains: p => {
      return (x + size / 2 - p.x) ** 2 + (y + size / 2 - p.y) ** 2 <= size ** 2 / 4
    },
    translate: (dx, dy) => {
      // if (y + dy >= 50) {
      x += dx
      y += dy
      // }
    },
    getColor: () => {
      return color
    },
    changeColor(newColor) {
        color = newColor
    },
    // draw: () => {
    //   const canvas = document.getElementById('graphpanel')
    //   const ctx = canvas.getContext('2d')
    //   ctx.beginPath()
    //   ctx.arc(x, y, size/2, 0, 360, false)
    //   ctx.fillStyle = color
    //   ctx.fill()
    // }
    draw () {
      const container = document.getElementById('nodeContainer')
      const table = document.createElement('table')
      const tr = document.createElement('tr')
      const td = document.createElement('td')
      table.appendChild(tr)
      tr.appendChild(td)
      table.style.position = 'absolute'
      table.style.left = x + 'px'
      table.style.top = y + 'px'
      table.style.width = size + 'px'
      table.style.height = size + 'px'
      table.style.background = color
      container.appendChild(table)
    },
    getType: () => {
      return 'parent'
    },
    getSize: () => {
      return size
    },
    changeSize(newSize) {
        size = newSize
    },
    drawTool (container) {
      const table = document.createElement('table')
      const tr = document.createElement('tr')
      const td = document.createElement('td')
      table.appendChild(tr)
      tr.appendChild(td)
      table.style.position = 'absolute'
      table.style.left = '45%'
      table.style.top = '35%'
      table.style.width = size + 'px'
      table.style.height = size + 'px'
      table.style.background = color
      container.appendChild(table)
    }
  }
}

function createRectangleNode (x, y, width1, height1, color, words) {
  let array = []
  return {
    getBounds: () => {
      return {
        x: x + 50,
        y: y,
        width: width1,
        height: height1
      }
    },
    contains: p => {
      return (x + width1 / 2 - p.x) ** 2 + (y + width1 / 2 - p.y) ** 2 <= width1 ** 2 / 4
    },
    translate: (dx, dy) => {
      x += dx
      y += dy
    },
    addText: (newText) => {
      array.push(newText)
    },
    draw () {
      const container = document.getElementById('nodeContainer')
      const table = document.createElement('table')
      var tblBody = document.createElement('tbody')
      // const tr = document.createElement('tr')
      // const td = document.createElement('td')
      // array.push(words)
      for (let i = 0; i < array.length; i++) {
        var tr = document.createElement('tr')
        var td = document.createElement('td')
        const tempText = document.createTextNode(array[i])
        // td.appendChild(tempText)
        td.appendChild(tempText)
        tr.appendChild(td)
        table.appendChild(tr)
      }
      // td.id='rectangle1'
      // table.appendChild(tr)
      // tr.appendChild(td)
      table.style.position = 'absolute'
      table.style.left = x + 'px'
      table.style.top = y + 'px'
      table.style.width = width1 + 'px'
      table.style.height = height1 + 'px'
      table.style.background = color
      container.appendChild(table)
      // FontRenderContext context = new FontRenderContext() {};
      // Rectangle2D bounds = font.getStringBounds(text, context);
      // bounds.getWidth() and bounds.getHeight() give the width and height
      // of the string in the given font
      // g2.setFont(font);
      // g2.drawString(string, (int) x, (int) (y - bounds.getY())); 
      // draws the string with upper left corner (x, y)
    },

    drawTool (container) {
      const table = document.createElement('table')
      var tblBody = document.createElement('tbody')

      // const tr = document.createElement('tr')
      // const td = document.createElement('td')
      // array.push(words)
      for (let i = 0; i < array.length; i++) {
        var tr = document.createElement('tr')
        var td = document.createElement('td')
        const tempText = document.createTextNode(array[i])
        // td.appendChild(tempText)
        td.appendChild(tempText)
        tr.appendChild(td)
        table.appendChild(tr)
      }
      // td.id='rectangle1'
      // table.appendChild(tr)
      // tr.appendChild(td)
      table.style.position = 'absolute'
      table.style.left = x + 'px'
      table.style.top = y + 'px'
      table.style.width = width1 + 'px'
      table.style.height = height1 + 'px'
      table.style.background = color
      container.appendChild(table)
    },
    getType: () => {
      return 'parent'
    }
  }
}

function createToolBar () {
  let nodes = []
  let selection
  return {
    draw: () => {
      /* const canvas = document.getElementById('toolpanel')
      const ctx = canvas.getContext('2d')
      ctx.beginPath()
      ctx.strokeRect(0, 0, 400, 75)
      for (const n of nodes) {
        n.drawTool()
      } */
      const container = document.getElementById('toolContainer')
      for (let i = 0; i < nodes.length; i++) {
        const toolDiv = document.createElement('div')
        toolDiv.id = 'div' + i
        const toolCanv = document.createElement('canvas')
        toolDiv.className = 'toolDiv'
        toolCanv.className = 'toolCanv'
        toolCanv.width = (toolCanv.width * 0.5)
        toolCanv.height = (toolCanv.height * 0.5)
        container.appendChild(toolDiv)
        toolDiv.appendChild(toolCanv)
        // toolDiv.addEventListener('click', tools.changeSelection(nodes[i]))
        if (nodes[i].getType() === 'parent') {
          nodes[i].drawTool(toolDiv)
        } else if (nodes[i].getType() === 'edge') {
          nodes[i].drawTool(toolCanv)
        }
      }
    },
    add: (n) => {
      nodes.push(n)
    },
    findNode: (p) => {
      for (const n of nodes) {
        if (n.contains(p)) return n
      }
      return undefined
    },
    changeSelection: (selected) => {
      selection = selected
    },
    /* drawSelection: (dragStartPoint) => {
      selection.draw(dragStartPoint)
    }, */
    getSelection: () => {
      return selection
    },
    getNodes: () => {
      return nodes
    }
  }
}

class Graph {
  constructor () {
    this.nodes = []
    this.edges = []
  }
  add (n) {
    this.nodes.push(n)
  }
  findNode (p) {
    for (let i = this.nodes.length - 1; i >= 0; i--) {
      const n = this.nodes[i]
      if (n.contains(p)) return n
    }
    return undefined
  }
  draw () {
    for (const n of this.nodes) {
      n.draw()
    }
    for (const e of this.edges) {
      e.draw()
    }
  }

  connect (e, p1, p2) {
    const n1 = p1
    const n2 = p2
    if (n1 !== undefined && n2 !== undefined) {
      e.connect(n1, n2)
      this.edges.push(e)
      return true
    }
    return false
  }
}
function center (rect) {
  return { x: rect.x + rect.width / 2, y: rect.y + rect.height / 2 }
}

function createLineEdge () {
  let start
  let end
  return {
    connect: (s, e) => {
      start = s
      end = e
    },
    draw: () => {
      const canvas = document.getElementById('graphpanel')
      const ctx = canvas.getContext('2d')
      ctx.beginPath()
      // Just pick the center of the bounds for now
      const p = center(start.getBounds())
      // Not the "connection points" that graphed2 uses
      const q = center(end.getBounds())
      ctx.moveTo(p.x, p.y)
      ctx.lineTo(q.x, q.y)
      ctx.stroke()
    },
    getType: () => {
      return 'edge'
    },
    drawTool: (canvas) => {
      const ctx = canvas.getContext('2d')
      ctx.transform(0.5, 0, 0, 0.5, 0, 0)
      ctx.beginPath()
      ctx.moveTo(0, canvas.height)
      ctx.lineTo(canvas.width, 0)
      ctx.stroke()
    },
    contains: (p) => {
    },
    clone: () => {
      return createLineEdge()
    }
  }
}

function createDottedLineEdge () {
  let start
  let end
  return {
    connect: (s, e) => {
      start = s
      end = e
    },
    draw: () => {
      const canvas = document.getElementById('graphpanel')
      const ctx = canvas.getContext('2d')
      ctx.beginPath()
      // Just pick the center of the bounds for now
      const p = center(start.getBounds())
      // Not the "connection points" that graphed2 uses
      const q = center(end.getBounds())
      ctx.setLineDash([6, 9]);
      ctx.moveTo(p.x, p.y)
      ctx.lineTo(q.x, q.y)
      ctx.stroke()
    },
    getType: () => {
      return 'edge'
    },
    drawTool: (canvas) => {
      const ctx = canvas.getContext('2d')
      ctx.transform(0.5, 0, 0, 0.5, 0, 0)
      ctx.beginPath()
      ctx.moveTo(0, canvas.height)
      ctx.lineTo(canvas.width, 0)
      ctx.stroke()
    },
    contains: (p) => {
    },
    clone: () => {
      return createDottedLineEdge()
    }
  }
}

function createCurvedLineEdge() {
  let start
  let end
  return {
    connect: (s, e) => {
      start = s
      end = e
    },
    draw: () => {
      const canvas = document.getElementById('graphpanel')
      const ctx = canvas.getContext('2d')
      ctx.beginPath()
      // Just pick the center of the bounds for now
      const p = center(start.getBounds())
      // Not the "connection points" that graphed2 uses
      const q = center(end.getBounds())
      ctx.moveTo(p.x, p.y)
      ctx.bezierCurveTo(p.x + 50, p.y + 50, q.x - 50, q.y - 50, q.x, q.y);
      ctx.stroke()
    },
    getType: () => {
      return 'edge'
    },
    drawTool: (canvas) => {
      const ctx = canvas.getContext('2d')
      ctx.transform(0.5, 0, 0, 0.5, 0, 0)
      ctx.beginPath()
      ctx.moveTo(0, canvas.height)
      ctx.lineTo(canvas.width, 0)
      ctx.stroke()
    },
    contains: (p) => {
    },
    clone: () => {
      return createCurvedLineEdge()
    }
  }
}

document.addEventListener('DOMContentLoaded', function () {
  const graph = new Graph()
  //  ADDING TOOLS
  const blueCircle = createCircleNode(0, 0, 30, 'blue')
  const goldenCircle = createCircleNode(0, 0, 30, 'goldenrod')
  const rect1 = createRectangleNode(10, 20, 30, 20, 'grey', 'txt')
  const rect2 = createRectangleNode(10, 20, 30, 20, 'red', 'txt')
  const curveEdge = createCurvedLineEdge()
  const lineEdge = createLineEdge()
  const dotEdge = createDottedLineEdge()
  const tools = createToolBar()
  /*  // tools.add(rect1)
  graph.add(rect1)
  graph.add(rect2)
  rect2.addText('New')
  rect2.addText('yei')
  rect2.addText('The longest path')
 */
  graph.draw()
  // tools.add(rect2)
  // tools.add(edgeTool)
  // tools.add(rect2)
  tools.add(blueCircle)
  tools.add(goldenCircle)
  tools.add(curveEdge)
  tools.add(lineEdge)
  tools.add(dotEdge)


  graph.draw()
  tools.draw()
  let nodes = tools.getNodes()
  for (let i = 0; i < nodes.length; i++) {
    const toolDiv = document.getElementById('div' + i)
    toolDiv.addEventListener('click', () => tools.changeSelection(nodes[i]))
  }
  const panel = document.getElementById('graphpanel')
  let selected
  let dragStartPoint
  let dragStartBounds
  let startNode
  let endNode
  function repaint () {
    const container = document.getElementById('nodeContainer')
    const canvas = document.getElementById('graphpanel')
    const ctx = canvas.getContext('2d')
    ctx.clearRect(0, 0, canvas.width, canvas.height)
    container.innerHTML = ''
    graph.draw()
    // tools.draw()
    if (selected !== undefined) {
      const bounds = selected.getBounds()
      drawGrabber(bounds.x, bounds.y)
      drawGrabber(bounds.x + bounds.width, bounds.y)
      drawGrabber(bounds.x, bounds.y + bounds.height)
      drawGrabber(bounds.x + bounds.width, bounds.y + bounds.height)
    }
  }
  function mouseLocation (event) {
    var rect = panel.getBoundingClientRect()
    return {
      x: event.clientX - rect.left,
      y: event.clientY - rect.top
    }
  }
  function applyButton() {
    let newColor = document.getElementById("colorBox").value
    let newSize = document.getElementById("sizeBox").value
    selected.changeSize(newSize)
    selected.changeColor(newColor)
    repaint()
  }
  panel.addEventListener('mousedown', event => {
    const mousePoint = mouseLocation(event)
    selected = graph.findNode(mousePoint)
    startNode = selected
    if (selected !== undefined) {
      dragStartPoint = mousePoint
      dragStartBounds = selected.getBounds()
    } else if (tools.getSelection().getType() === 'parent') {
      let selection = tools.getSelection()
      let bounds = selection.getBounds()
      const temp = createCircleNode(mousePoint.x, mousePoint.y, bounds.width, selection.getColor())
      graph.add(temp)
    } else if (tools.getSelection().getType() === 'edge') {
      startNode = selected
    }
    // removing propertysheet when user clicks elsewhere
    var myNode = document.getElementById("propertySheet");
    while (myNode.firstChild) {
      myNode.removeChild(myNode.firstChild);
    }
    repaint()
  })
  panel.addEventListener('dblclick', event => { // property sheet eventlistener
    // for when you doubleclick a node
    const mousePoint = mouseLocation(event)
    selected = graph.findNode(mousePoint)
    if (selected !== undefined) {   // if you dblclicked a node
      //creates node color descriptor
      const y = document.createElement("p")
      const z = document.createTextNode("Node Color: ")
      y.appendChild(z)
      document.getElementById('propertySheet').appendChild(y)
      
      // creates input text box for color
      const x = document.createElement("input")
      x.setAttribute("type", "text")
      x.setAttribute("id", "colorBox")
      x.setAttribute("value", selected.getColor())
      x.addEventListener("keydown", applyButton)
      x.addEventListener("keyup", applyButton)
      document.getElementById('propertySheet').appendChild(x)

       //creates node size descriptor
      const size = document.createElement("p")
      const sizeText = document.createTextNode("Node Size: ")
      size.appendChild(sizeText)
      document.getElementById('propertySheet').appendChild(size)
      
      // creates input text box for size
      const sBox = document.createElement("input")
      sBox.setAttribute("type", "text")
      sBox.setAttribute("id", "sizeBox")
      sBox.setAttribute("value", selected.getSize())
      sBox.addEventListener("keydown", applyButton)
      sBox.addEventListener("keyup", applyButton)
      document.getElementById('propertySheet').appendChild(sBox)

      // add linebreak for button
      const br = document.createElement("br")
      document.getElementById('propertySheet').appendChild(br)
      
    /*
      // creates button that applies changes
      const tempButton = document.createElement("input")
      tempButton.setAttribute("type", "button")
      tempButton.setAttribute("value", "Apply")
      // add event listener to the button 
      tempButton.addEventListener("click", applyButton)
      document.getElementById('propertySheet').appendChild(tempButton)
    */
    }
  })

  /*  tpanel.addEventListener('mousedown', event => {
    let mousePoint = tmouseLocation(event)
    selected = tools.findNode(mousePoint)
    if (selected !== undefined) {
      if (selected.getType() === 'circle') {
        const newNode = createCircleNode(200, 200, 20, selected.getColor())// creates a new node, this is just placeholder
        graph.add(newNode) // ideally in the future we can figure out the kind of node then create it
      }
    }
    repaint()
  }) */

  /*  tpanel.addEventListener('mouseup', event => {
    dragStartPoint = undefined
    dragStartBounds = undefined
  }) */
  panel.addEventListener('mousemove', event => {
    if (dragStartPoint === undefined) return
    let mousePoint = mouseLocation(event)
    if (selected !== undefined && tools.getSelection().getType() !== 'edge') {
      const bounds = selected.getBounds()
      selected.translate(
        dragStartBounds.x - bounds.x +
        mousePoint.x - dragStartPoint.x,
        dragStartBounds.y - bounds.y +
        mousePoint.y - dragStartPoint.y)
      repaint()
    }
  })
  panel.addEventListener('mouseup', event => {
    let mousePoint = mouseLocation(event)
    selected = graph.findNode(mousePoint)
    if (selected !== undefined && selected !== startNode && tools.getSelection().getType() === 'edge') {
      endNode = selected
      graph.connect(tools.getSelection().clone(), startNode, endNode)
    }
    dragStartPoint = undefined
    dragStartBounds = undefined
    repaint()
  })
})